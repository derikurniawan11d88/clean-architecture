package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/derikurniawan11d88/clean-architecture/boot"
	"gitlab.com/derikurniawan11d88/clean-architecture/libraries/utils"
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse"
	"log"
	"os"
	"runtime"
)

const (
	// ConfigFile
	//   Default path of config file directory
	ConfigFile = "./config.yaml"

	// RedisCacheConfig
	//     Redis cache config from file after parsed
	RedisCacheConfig = "db.redis.cache_db"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// Config File parser. config file may be type of yaml.
	// Please do copy as config example
	config := utils.NewConfig(ConfigFile)

	// boot options for setting
	// available for another optional setting
	var option boot.Options
	option.Redis.DB = config.GetInt(RedisCacheConfig)
	option.Redis.Timeout = 100
	option.Redis.MaxTries = 10

	// Add new service here
	// this AddService implements Service methods
	// boot.Setup has 2 generic types,
	//   1. SQL relational database connection as 'T' variable type
	//   2. No SQL object mapping database connection as 'X' variable type
	// Set the connection in main function
	app := boot.Setup(config, option)
	app.AddService(warehouse.Boot(app), "warehouse", "Warehouse Services")

	cmd := &cli.App{
		Name:     "Clean API Core",
		Usage:    "Clean API Core, cli",
		Commands: app.ServiceCommand,
		Action: func(cli *cli.Context) error {
			fmt.Printf("%s version@%s\n", cli.App.Name, "1.1")
			return nil
		},
	}

	err := cmd.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
