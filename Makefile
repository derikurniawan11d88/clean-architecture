WAREHOUSE_WORKSPACE := ./services/warehouse

protoc_product:
	protoc --proto_path=$(WAREHOUSE_WORKSPACE)/protos $(WAREHOUSE_WORKSPACE)/protos/product.proto --go_out=$(WAREHOUSE_WORKSPACE) --go-grpc_out=$(WAREHOUSE_WORKSPACE)
	@echo "Product protoc success"