package boot

import (
	"github.com/go-redis/redis/v8"
	"github.com/urfave/cli/v2"
	"gitlab.com/derikurniawan11d88/clean-architecture/libraries/database"
	"gitlab.com/derikurniawan11d88/clean-architecture/libraries/logger"
	"gitlab.com/derikurniawan11d88/clean-architecture/libraries/utils"
	"time"
)

type (
	// App
	/*
	 *	App Contract master for application
	 *		Define this for connection setting for running any services
	 */
	App struct {
		Debug          bool
		DB             database.Methods
		Config         utils.Config
		Cache          *redis.Client
		ServiceCommand []*cli.Command
		Validator      *utils.Validator
		Logger         logger.Logger
	}

	// Service
	/*
	 *	Service method implementations
	 */
	Service interface {
		Start(c *cli.Context) error
		CommandFlag() []cli.Flag
	}

	Options struct {
		Redis struct {
			Address  string
			Password string
			DB       int
			Timeout  int
			MaxTries int
		}
	}
)

func Setup(config utils.Config, options ...Options) *App {
	app := App{}

	opt := Options{}
	if len(options) > 0 {
		opt = options[0]
	}

	app.Debug = config.GetBool("app.debug")
	app.Config = config
	app.Validator = utils.SetupValidator(config)

	if len(opt.Redis.Address) < 1 {
		opt.Redis.Address = "127.0.0.1:6379"
	}

	if opt.Redis.Timeout < 1 {
		opt.Redis.Timeout = 60
	}

	if opt.Redis.MaxTries < 1 {
		opt.Redis.MaxTries = 5
	}

	app.Cache = redis.NewClient(&redis.Options{
		Addr:            opt.Redis.Address,
		Password:        opt.Redis.Password,
		DialTimeout:     time.Second * time.Duration(opt.Redis.Timeout),
		ReadTimeout:     time.Second * time.Duration(opt.Redis.Timeout),
		WriteTimeout:    time.Second * time.Duration(opt.Redis.Timeout),
		MaxRetries:      opt.Redis.MaxTries,
		MaxRetryBackoff: time.Second * time.Duration(opt.Redis.MaxTries),
		DB:              opt.Redis.DB,
	})

	app.Logger = logger.New()

	return &app
}

func (app *App) AddService(serve Service, name, desc string) {
	app.ServiceCommand = append(app.ServiceCommand, &cli.Command{
		Name:   name,
		Usage:  desc,
		Flags:  serve.CommandFlag(),
		Action: serve.Start,
	})
}
