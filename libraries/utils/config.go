package utils

import (
	"github.com/spf13/viper"
	"strings"
)

type (
	Config interface {
		GetText(string) string
		GetInt(string) int
		GetBool(string) bool
		GetInt64(string) int64
	}

	viperConfig struct{}
)

func NewConfig(configPath string) Config {
	viper.SetConfigFile("yaml")
	viper.SetEnvPrefix("app_rpc_config")
	viper.AutomaticEnv()

	viper.SetConfigFile(configPath)
	viper.EnvKeyReplacer(strings.NewReplacer(".", "/"))

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	return &viperConfig{}
}

func (vc *viperConfig) GetText(key string) string {
	return viper.GetString(key)
}

func (vc *viperConfig) GetBool(key string) bool {
	return viper.GetBool(key)
}

func (vc *viperConfig) GetInt(key string) int {
	return viper.GetInt(key)
}

func (vc *viperConfig) GetInt64(key string) int64 {
	return viper.GetInt64(key)
}
