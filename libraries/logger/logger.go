package logger

import (
	"github.com/sirupsen/logrus"
	"os"
)

type Logger interface {
	File(string) *logrus.Logger
}

type logx struct {
	Logrus *logrus.Logger
}

func New() Logger {
	return &logx{}
}

func (lg *logx) File(source string) *logrus.Logger {
	var (
		err  error
		file *os.File
	)

	if _, err = os.Stat(source); err == nil {
		file, err = os.OpenFile(source, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err == nil {
			lg.Logrus.Out = file

			return lg.Logrus
		}
	}

	file, err = os.OpenFile(source, os.O_CREATE|os.O_WRONLY, 0755)
	if err == nil {
		lg.Logrus.Out = file
	} else {
		lg.Logrus.Info("Failed to log to file, using default stderr")
	}

	defer file.Close()

	return lg.Logrus
}
