package database

import "github.com/elastic/go-elasticsearch/v7"

type (
	// ElasticSearch
	/*
	 *	Struct client for elastic search no sql database
	 *		Any setting about elastic search, you can input on config
	 */
	ElasticSearch struct {
		Client *elasticsearch.Client
	}
)
