package database

import "go.mongodb.org/mongo-driver/mongo"

type (
	// MongoDB
	/*
	 *	Struct client for MongoDb no sql database
	 *		Any setting about mongo, you can input on config
	 */
	MongoDB struct {
		Client *mongo.Client
	}
)
