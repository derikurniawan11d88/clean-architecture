package database

import "github.com/jmoiron/sqlx"

type (
	// DB
	/*
	 *	Struct pool for relational database systems, such as MySQL
	 *		Define your connection on config file before use it, and define this after connect
	 */
	DB struct {
		Pool *sqlx.DB
	}
)
