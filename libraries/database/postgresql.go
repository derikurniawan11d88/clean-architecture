package database

import "github.com/jackc/pgx/v5/pgxpool"

type (
	// PostgresQL
	/*
	 *	Struct pool for database relational of Postgre
	 *		Define your connection on config file before use it, and define this after connect
	 */
	PostgresQL struct {
		Pool *pgxpool.Pool
	}
)
