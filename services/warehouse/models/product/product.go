package product

import (
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/models"
)

var (
	Measures = [...]string{
		"Packages",
		"Peaces",
		"Batang",
		"Kardus",
	}

	ProdStatus = [...]string{
		"not_active",
		"active",
		"pending",
		"removed",
	}

	Units = [...]string{
		"Small",
		"Medium",
		"Big",
	}
)

type (
	// Entity
	/*
	 * Product entities untuk mendefined data-data product ke dalam database
	 * menggunakan database mongoDb maka tagging struct menggunakan 'bson'
	 */
	Entity[A models.ID, B models.Text, C models.Time] struct {
		ID          A         `db:"id" bson:"_id,omitempty"`
		Name        string    `db:"name" bson:"name"`
		Code        string    `db:"code" bson:"code"`
		Measurement Measure   `db:"measurement" bson:"measurement"`
		UnitType    UnitTyped `db:"unit_type" bson:"unit_type"`
		Status      Status    `db:"status" bson:"status"`
		Description B         `db:"description" bson:"description"`
		CreatedAt   C         `db:"created_at" bson:"created_date"`
		UpdatedAt   C         `db:"updated_at" bson:"updated_date"`
		DeletedAt   C         `db:"deleted_at" bson:"deleted_date"`
	}

	Filter struct {
		Codes  []string `json:"codes"`
		Name   string   `json:"name"`
		Status string   `json:"status"`
		Page   int      `json:"page"`
		Limit  int      `json:"limit"`
	}

	Measure   int
	Status    int
	UnitTyped int

	Model[A models.ID, B models.Text, C models.Time] interface {
		// InsertNewProduct
		//	Insert product baru dengan kembalian error
		//	data parameter berupa pointer, untuk memudahkan user mendapatkan
		//  data product baru beserta id-nya
		InsertNewProduct(*Entity[A, B, C]) error

		// InsertBulkProduct
		//	Insert product data baru dengan sekaligus
		//  Ini akan mengembalikan seberapa banyak data yang berhasil di insert
		//		@returns NumberInserted<int>, err<error>
		InsertBulkProduct([]Entity[A, B, C]) (int, error)

		// GetProductByCode
		//	Mendapatkan data product dari database dengan parameter code product
		//	Data entity dari product akan diberikan jika result tidak error
		GetProductByCode(string) (Entity[A, B, C], error)

		// GetListProducts
		//	Mendapatkan data-data product dari database dengan param filter
		GetListProducts(Filter) ([]Entity[A, B, C], error)
	}
)

const (
	// Measurement ENUM entries
	NO_DEFINED Measure = iota - 1
	PKG
	PCS
	BTG
	BOX
)

func (m *Measure) String() string {
	return Measures[*m-1]
}

func (m *Measure) Parse(tipe string) {
	tipes := make(map[string]Measure)
	for i, meas := range Measures {
		tipes[meas] = Measure(i)
	}

	*m = NO_DEFINED
	if v, ok := tipes[tipe]; ok {
		*m = v
	}
}

const (
	// Product status ENUM lists
	NO_REGIST Status = iota - 1
	NO_ACTIVE
	ACTIVE
	PENDING
	REMOVED
)

func (s *Status) String() string {
	return ProdStatus[*s-1]
}

func (s *Status) Parse(tipe string) {
	types := make(map[string]Status)
	for i, st := range ProdStatus {
		types[st] = Status(i)
	}

	*s = NO_REGIST
	if v, ok := types[tipe]; ok {
		*s = v
	}
}

const (
	// Product unit type ENUM lists
	NO_UNIT UnitTyped = iota - 1
	SMALL
	MEDIUM
	BIG
)

func (u *UnitTyped) String() string {
	return Units[*u-1]
}

func (u *UnitTyped) Parse(tipe string) {
	types := make(map[string]UnitTyped)
	for i, st := range ProdStatus {
		types[st] = UnitTyped(i)
	}

	*u = NO_UNIT
	if v, ok := types[tipe]; ok {
		*u = v
	}
}
