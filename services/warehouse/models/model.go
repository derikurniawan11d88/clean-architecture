package models

import (
	"database/sql"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type (
	ID interface {
		primitive.ObjectID | sql.NullInt64 | int64
	}

	Text interface {
		sql.NullString | string
	}

	Number32 interface {
		sql.NullInt32 | int32
	}

	Number64 interface {
		sql.NullInt64 | int64
	}

	Float interface {
		sql.NullFloat64 | float64
	}

	Bool interface {
		sql.NullBool | bool
	}

	Time interface {
		sql.NullTime | time.Time
	}
)
