package handler

import (
	"context"
	"database/sql"
	"gitlab.com/derikurniawan11d88/clean-architecture/boot"
	"gitlab.com/derikurniawan11d88/clean-architecture/libraries/database"
	proto "gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/buffers/product"
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/controllers"
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/repository"
	"google.golang.org/grpc"
)

type (
	Handler struct {
		proto.UnimplementedProductServiceServer
		app *boot.App
	}
)

func ProductRoute(serve *grpc.Server, app *boot.App) {
	proto.RegisterProductServiceServer(serve, &Handler{app: app})
}

func (h *Handler) GetProductList(c context.Context, p *proto.ProductList) (*proto.ProductResponse, error) {
	return nil, nil
}

func (h *Handler) GetProductByCode(c context.Context, p *proto.ProductCode) (*proto.ProductResponse, error) {
	var (
		mgb  = database.MongoDB{}.Client
		repo = repository.NewProduct[int64, sql.NullString, sql.NullTime](mgb, h.app.DB, c, h.app.Cache)

		control = controllers.CallProduct(repo)
		resp    proto.ProductResponse

		data = proto.Product{}
	)

	result, err := control.GetProductByCode(p.GetCode())
	if err != nil {
		return &resp, err
	}

	data.Code = result.Code
	data.Name = result.Name
	data.Status = result.Status
	data.Description = result.Description
	data.Measurement = result.Measurement
	data.UnitType = result.UnitType

	resp.Data = append(resp.Data, &data)
	return &resp, nil
}

func (h *Handler) CreateProduct(c context.Context, p *proto.Product) (*proto.ProductResponse, error) {
	return nil, nil
}

func (h *Handler) DeleteProduct(c context.Context, p *proto.ProductCode) (*proto.ProductResponse, error) {
	return nil, nil
}

func (h *Handler) UpdateProduct(c context.Context, p *proto.Product) (*proto.ProductResponse, error) {
	return nil, nil
}
