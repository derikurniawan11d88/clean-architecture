package warehouse

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/derikurniawan11d88/clean-architecture/boot"
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/handler"
	"google.golang.org/grpc"
	"log"
	"net"
)

type (
	Contract struct {
		app *boot.App
	}
)

func Boot(app *boot.App) boot.Service {
	return &Contract{app: app}
}

func (c *Contract) CommandFlag() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:  "host",
			Value: "127.0.0.1:50050",
			Usage: "Run service warehouse",
		},
	}
}

func (c *Contract) Start(ctx *cli.Context) error {
	host := ctx.String("host")
	if len(host) == 0 {
		host = c.app.Config.GetText("app.host")
	}

	listener, er := net.Listen("tcp", host)
	if er != nil {
		return er
	}

	server := grpc.NewServer()
	handler.ProductRoute(server, c.app)

	log.Printf("Server started at %v\n", listener.Addr())
	if err := server.Serve(listener); err != nil {
		log.Printf("Failed serve warehouse service: %v\n", err)
		return err
	}

	return nil
}
