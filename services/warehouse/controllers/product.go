package controllers

import (
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/models"
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/models/product"
	"time"
)

type (
	productController[A models.ID, B models.Text, C models.Time] struct {
		repo product.Model[A, B, C]
	}

	Product interface {
		GetProductByCode(string) (Reserved, error)
		InsertNewProduct(Payload) error
	}

	// Reserved
	//	This payload results from product controller
	Reserved struct {
		Code        string    `json:"code"`
		Name        string    `json:"name"`
		Measurement string    `json:"measurement"`
		UnitType    string    `json:"unit_type"`
		Status      string    `json:"status"`
		Description string    `json:"description"`
		CreatedDate time.Time `json:"createdDate"`
		DeletedDate time.Time `json:"deletedDate"`
	}

	// Payload
	//   For product controller(UseCase)
	//   this is can be invoked request from handler
	Payload struct {
		Name        string `json:"name" validate:"required"`
		Measure     string `json:"measure" validate:"required"`
		UnitType    string `json:"unitType" validate:"required"`
		Status      string `json:"status" validate:"required"`
		Description string `json:"description"`
	}
)

func CallProduct[A models.ID, B models.Text, C models.Time](repo product.Model[A, B, C]) Product {
	return &productController[A, B, C]{repo: repo}
}

func (p productController[A, B, C]) GetProductByCode(code string) (Reserved, error) {
	var (
		pd = Reserved{}
	)
	return pd, nil
}

func (p productController[A, B, C]) InsertNewProduct(payload Payload) error {
	return nil
}
