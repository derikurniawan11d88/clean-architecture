package repository

import (
	"context"
	"github.com/go-redis/redis/v8"
	"gitlab.com/derikurniawan11d88/clean-architecture/libraries/database"
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/models"
	"gitlab.com/derikurniawan11d88/clean-architecture/services/warehouse/models/product"
	"go.mongodb.org/mongo-driver/mongo"
)

// check general type go playground
// https://go.dev/play/p/dj0-cHb5ylh
type (
	repProduct[A models.ID, B models.Text, C models.Time] struct {
		db      database.Methods
		mgdb    *mongo.Client
		context context.Context
		redisDB *redis.Client
	}
)

func NewProduct[A models.ID, B models.Text, C models.Time](
	mgdb *mongo.Client, pool database.Methods, ctx context.Context, rdb *redis.Client,
) product.Model[A, B, C] {
	return &repProduct[A, B, C]{
		db:      pool,
		mgdb:    mgdb,
		context: ctx,
		redisDB: rdb,
	}
}

func (rp *repProduct[A, B, C]) InsertNewProduct(p *product.Entity[A, B, C]) error {
	return nil
}

func (rp *repProduct[A, B, C]) InsertBulkProduct([]product.Entity[A, B, C]) (int, error) {
	return 0, nil
}

func (rp *repProduct[A, B, C]) GetProductByCode(code string) (product.Entity[A, B, C], error) {
	var (
		data = product.Entity[A, B, C]{}
	)
	return data, nil
}

func (rp *repProduct[A, B, C]) GetListProducts(filter product.Filter) ([]product.Entity[A, B, C], error) {
	var (
		data = make([]product.Entity[A, B, C], 0)
	)
	return data, nil
}
